<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.Latveducation
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

defined('_JEXEC') or die;

/** @var JDocumentHtml $this */

$app      = JFactory::getApplication();
$user     = JFactory::getUser();
$document = JFactory::getDocument();
$lang     = $this->language;

// Getting params from template
$params   = $app->getTemplate(true)->params;
$sitename = $app->get('sitename');

// Output as HTML5
$this->setHtml5(true);

$template_url = $this->baseurl . '/templates/' . $this->template;

$css_path = 'templates/' . $this->template . '/stylesheets/';
$js_path  = 'templates/' . $this->template . '/js/';

$input   = $app->input;
$id      = $input->getInt('id'); //get the article ID
$article = JTable::getInstance('content');
$article->load($id);

/*Stylesheets*/
JHtml::_('stylesheet', $css_path . 'bootstrap.css', array());
JHtml::_('stylesheet', $css_path . 'shortcodes.css', array());
JHtml::_('stylesheet', $css_path . 'style.css', array());
JHtml::_('stylesheet', $css_path . 'magnific-popup.css', array());
JHtml::_('stylesheet', $css_path . 'latveducation.css', array());
JHtml::_('stylesheet', $css_path . 'responsive.css', array());
JHtml::_('stylesheet', $css_path . 'colors/color1.css', array());

/*TOP javascript*/
JHtml::_('script', $js_path . 'html5shiv.js', array('version' => 'auto', 'relative' => true, 'conditional' => 'lt IE 9'));
JHtml::_('script', $js_path . 'respond.min.js', array('version' => 'auto', 'relative' => true, 'conditional' => 'lt IE 9'));

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}

unset($this->_scripts['/media/jui/js/bootstrap.js']);
?>


<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=$lang?>" lang="<?=$lang?>"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:jdoc="http://www.w3.org/1999/XSL/Transform" xml:lang="<?= $lang ?>"
      lang="<?= $lang ?>"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <script>
        var baseURL = "<?=$this->baseurl?>";
        var templateURL = "<?=$template_url?>;
    </script>

    <jdoc:include type="head"/>
</head>


<body class="header-sticky">
<div class="boxed">
    <!-- Preloader -->
    <div class="windows8">
        <div class="preload-inner">
            <div class="wBall" id="wBall_1">
                <div class="wInnerBall"></div>
            </div>
            <div class="wBall" id="wBall_2">
                <div class="wInnerBall"></div>
            </div>
            <div class="wBall" id="wBall_3">
                <div class="wInnerBall"></div>
            </div>
            <div class="wBall" id="wBall_4">
                <div class="wInnerBall"></div>
            </div>
            <div class="wBall" id="wBall_5">
                <div class="wInnerBall"></div>
            </div>
        </div>
    </div>

    <!-- Header inner pages-->
    <div class="header-inner-pages">
        <div class="top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-information">
                            <jdoc:include type="modules" name="Section menu position"/>
                        </div>
                        <div class="right-bar">
                            <jdoc:include type="modules" name="Top - right bar contacts"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.header-inner-pages -->

    <!-- Header -->
    <header id="header" class="header clearfix">
        <div class="container">
            <div class="header-wrap clearfix">
                <div id="logo" class="logo">
                    <a href="/" rel="home">
						<?= $logo ?>
                    </a>
                </div><!-- /.logo -->
                <div class="nav-wrap">
                    <div class="btn-menu">
                        <span></span>
                    </div><!-- //mobile menu button -->
                    <nav id="mainnav" class="mainnav">
                        <jdoc:include type="modules" name="Language menu position"/>
                        <jdoc:include type="modules" name="Custom menu position"/>
                    </nav><!-- /.mainnav -->
                </div><!-- /.nav-wrap -->

                <div id="s" class="show-search">
                    <a href="#"><i class="fa fa-search"></i></a>
                </div><!-- /.show-search -->

                <div class="submenu top-search">
                    <div class="widget widget_search">
                        <form class="search-form">
                            <input type="search" class="search-field" placeholder="Search …">
                            <input type="submit" class="search-submit">
                        </form>
                    </div>
                </div>
            </div><!-- /.header-inner -->
        </div>
    </header><!-- /.header -->

    <!-- Slider position -->
    <jdoc:include type="modules" name="Slider position"/>
    <!-- /Slider position -->

    <!-- Content -->
    <jdoc:include type="component"/>
    <!-- /Content -->

    <!-- Footer -->
    <footer class="footer">
        <div class="footer-widgets">
            <div class="container">
                <div class="row">
                    <div class="col-md-3"><jdoc:include type="modules" name="Footer block 1"/></div>
                    <div class="col-md-3"><jdoc:include type="modules" name="Footer block 2"/></div>
                    <div class="col-md-6"><jdoc:include type="modules" name="Footer block 3"/></div>
                </div>
            </div><!-- /.container -->
        </div><!-- /.footer-widgets -->
    </footer>

    <a class="go-top">
        <i class="fa fa-chevron-up"></i>
    </a>

    <!-- Bottom -->
    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="container-bottom">
                    <div class="copyright">
                        <p>© Latveducation SIA 2017.</p>
                    </div>
                </div><!-- /.container-bottom -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
</div><!-- /. boxed -->


<!-- Javascript -->

<!--<script type="text/javascript" src="<? /*=$template_url.'/javascript/jquery.min.js'*/ ?>"></script>-->
<script type="text/javascript" src="<?= $template_url . '/javascript/bootstrap.min.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/javascript/jquery.easing.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/javascript/owl.carousel.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/javascript/jquery-waypoints.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/javascript/jquery-countTo.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/javascript/parallax.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/javascript/jquery.cookie.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/javascript/jquery-validate.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/javascript/magnific-popup.min.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/javascript/main.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/javascript/le.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/mail/javascript/main.js' ?>"></script>


<!-- Revolution Slider -->
<script type="text/javascript" src="<?= $template_url . '/javascript/jquery.themepunch.tools.min.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/javascript/jquery.themepunch.revolution.min.js' ?>"></script>
<script type="text/javascript" src="<?= $template_url . '/javascript/slider.js' ?>"></script>


</body>
</html>
