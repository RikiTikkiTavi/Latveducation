/**
 * Created by root on 9/24/17.
 */

jQuery.noConflict();

jQuery(document).ready(function ($) {

    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function setCookie(name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }

    function getActiveSection() {
        var active_section = $("#menu-sections").find(".active").attr("site_section");
        if (typeof (active_section) == 'undefined') {
            return getCookie("active_site_section")
        }
        return active_section;
    }

    function setActiveSection(active_section) {
        var options = {
            path: "/"
        };
        var active_section_class = "." + active_section;
        var menu_sections = jQuery("#menu-sections");
        if (typeof page_category === 'undefined'){
            page_category = "general";
        } if (page_category === "Universities and Schools"){
            page_category = "Institutions";
        }
        if (page_category==="general") {
            if (!jQuery(menu_sections).find(active_section_class).hasClass("active")) {
                jQuery(menu_sections).find(active_section_class).addClass("active");
            }
        } else{
            if (!jQuery(menu_sections).find("a."+page_category).parent().hasClass("active")) {
                jQuery(menu_sections).find("a." + page_category).parent().addClass("active");
                setCookie("active_site_section", getActiveSection(), options);
            }
        }
        if (jQuery(".menuProjectsLong").length > 0) {
            jQuery(".menuProjectsLong li:first-child").addClass("active");
        }
    }

    function projectsMenuDropdown(){
        $('#static-nav .dropdown-toggle').click(function () {
            $('#static-nav .dropdown-toggle').dropdown();
        });

    }

    function pageInDev(){
        var content = $(".flat-row > .row > .col-md-12");
        if (content.length) {
            var is_in_dev = $(content).html().length === 0;
            if (is_in_dev) {
                $(content).html("<h3 class='text-center text-uppercase'>This page will be available soon</h3>");
            }
        }
    }

    function main() {

        var options = {
            path: "/"
        };

        setCookie("active_site_section", getActiveSection(), options);
        setActiveSection(getCookie("active_site_section"));
        projectsMenuDropdown();
        pageInDev();
    }

    main();


});

