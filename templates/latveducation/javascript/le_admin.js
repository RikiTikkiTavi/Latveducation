/**
 * Created by root on 9/20/17.
 */


jQuery.noConflict();

jQuery(document).ready(function($) {
   'use strict'

    var courseCarousel = function() {
        $('.flat-row').each(function() {
            if ( $().owlCarousel ) {
                $(this).find('.flat-course-grid').owlCarousel({
                    loop: true,
                    margin: 30,
                    nav: true,
                    dots: false,
                    autoplay: true,
                    responsive:{
                        0:{
                            items: 1
                        },

                        479:{
                            items: 2
                        },
                        991:{
                            items: 2
                        },
                        1200: {
                            items: 3
                        }
                    }
                });
            }
        });
    };

    // Dom Ready
    $(function() {
        courseCarousel();
    });

});