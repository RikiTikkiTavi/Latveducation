<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_languages
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('stylesheet', 'mod_languages/template.css', array('version' => 'auto', 'relative' => true));

if ($params->get('dropdown', 1) && !$params->get('dropdownimage', 0))
{
	JHtml::_('formbehavior.chosen');
}
?>
<ul class="menu lang-menu">
	<?php if ($params->get('dropdown', 1) && $params->get('dropdownimage', 0)) : ?>
		<?php foreach ($list as $language) : ?>
			<?php if ($language->active) : ?>
                <li class="has-sub">
                    <a href="#">
						<?= JHtml::_('image', 'mod_languages/' . $language->image . '.gif', $language->title_native, array('title' => $language->title_native), true); ?>
                    </a>
                    <ul class="submenu">
						<?php foreach ($list as $language) : ?>
							<?php if (!$language->active || $params->get('show_active', 0)) : ?>
                                <li>
                                    <a href="<?php echo $language->link; ?>">
										<?= JHtml::_('image', 'mod_languages/' . $language->image . '.gif', $language->title_native, array('title' => $language->title_native), true); ?>
                                    </a>
                                </li>
							<?php endif; ?>
						<?php endforeach; ?>
                    </ul>
                </li>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
</ul>
