<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_tags
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Note that there are certain parts of this layout used only when there is exactly one tag.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
$isSingleTag = count($this->item) === 1;
?>
<div class="flat-row">
    <div class="container">
        <div class="flat-title-section">
            <h1 class="title"><?php echo $this->tags_title; ?></h1>
        </div>
        <div class="tag-category<?php echo $this->pageclass_sfx; ?>">
			<?php echo $this->loadTemplate('items'); ?>
        </div>
    </div>
</div>
