<?php
// No direct access
defined('_JEXEC') or die;

/** @var object $fields */
?>

<ul class="flat-information">
    <li class="phone">
        <a href="<?=$fields -> phone_lv?>" title="Phone number"><?=$fields -> phone_lv?></a>
    </li>
    <li class="email">
        <a href="mailto:<?=$fields -> mail_lv?>" title="Email address"> <?=$fields -> mail_lv?></a>
    </li>
</ul>
<ul class="flat-socials">
    <li class="facebook">
        <a href="<?= $fields->facebook ?>">
            <i class="fa fa-facebook"></i>
        </a>
    </li>
    <li class="twitter">
        <a href="<?= $fields->twitter ?>">
            <i class="fa fa-twitter"></i>
        </a>
    </li>
    <li class="linkedin">
        <a href="<?= $fields->social_1 ?>">
            <i class="fa fa-linkedin"></i>
        </a>
    </li>
    <li class="youtube">
        <a href="<?= $fields->youtube ?>">
            <i class="fa fa-youtube-play"></i>
        </a>
    </li>
</ul>