<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$id = '';

if ($tagId = $params->get('tag_id', ''))
{
	$id = ' id="' . $tagId . '"';
}

$hidden_items = [];
$shown_items  = [];

foreach ($list as $i => &$item)
{

    $is_active = $item->id == $active_id || ($item->type === 'alias' && $item->params->get('aliasoptions') == $active_id);
    $is_first = $i == 0;

	if ($is_active || $is_first)
	{
		array_push($shown_items, $item);
	}
	else
	{
		array_push($hidden_items, $item);
	}
}
?>

<div id="static-nav" class="nav nav-pills nav-stacked menuProjectsShort">
	<?php foreach ($shown_items as $i => &$item)
	{

		$class = 'item-' . $item->id;

		if ($item->id == $default_id)
		{
			$class .= ' default';
		}

		if ($i == 0)
		{
			$class .= ' li-bold';
			echo '<li class="' . $class . '">';
		}

		if ($item->id == $active_id || ($item->type === 'alias' && $item->params->get('aliasoptions') == $active_id))
		{
			$class .= ' active';
			echo '<li class="' . $class . '">';
		}


		switch ($item->type) :
			case 'separator':
			case 'component':
			case 'heading':
			case 'url':
				require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
				break;

			default:
				require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
				break;
		endswitch;

		// The next item is on the same level.
		echo '</li>';
	}
	?>
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"><span
                    class="masha_index masha_index3" rel="3"></span>
            Other projects <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
			<?php foreach ($hidden_items as $i => &$item)
			{
				$class = 'item-' . $item->id;

				if ($item->id == $default_id)
				{
					$class .= ' default';
				}

				if ($item->id == $active_id || ($item->type === 'alias' && $item->params->get('aliasoptions') == $active_id))
				{
					$class .= ' active';
				}

				echo '<li class="' . $class . '">';

				switch ($item->type) :
					case 'separator':
					case 'component':
					case 'heading':
					case 'url':
						require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
						break;

					default:
						require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
						break;
				endswitch;

				// The next item is on the same level.
				echo '</li>';
			}
			?>
        </ul>
    </li>
</div>
