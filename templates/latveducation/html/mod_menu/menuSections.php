<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// The menu class is deprecated. Use nav instead
?>


<ul id="menu-sections">
	<?php foreach ($list as $i => &$item)
	{
		$class = 'item-' . $item->id;

		if ($item->id == $default_id)
		{
			$class .= ' default';
		}
		if ($item->id == $active_id || ($item->type === 'alias' && $item->params->get('aliasoptions') == $active_id))
		{
			$class .= ' active';
		}

		echo '<li site_section="' . 'item-' . $item->id . '" class="' . $class . '">';

		switch ($item->type) :
			case 'separator':
			case 'component':
			case 'heading':
			case 'url':
				require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
				break;

			default:
				require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
				break;
		endswitch;

		// The next item is on the same level.
		echo '</li>';
	}
	?>
</ul>
