<?php
/**
 * @package     ${NAMESPACE}
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

defined('_JEXEC') or die;

?>
<div class="sidebar">
    <div class="widget widget-categories">
		<?php
		if ($module->showtitle)
		{
			echo '<h5 class="widget-title">' . $module->title . '</h5>';
		}
		?>

        <ul>

			<?php foreach ($list as $i => &$item)
			{
				$class = 'item-' . $item->id;

				if ($item->id == $default_id)
				{
					$class .= ' default';
				}
				if ($item->id == $active_id || ($item->type === 'alias' && $item->params->get('aliasoptions') == $active_id))
				{
					$class .= ' active';
				}

				echo '<li class="' . $class . '">';

				switch ($item->type) :
					case 'separator':
					case 'component':
					case 'heading':
					case 'url':
						require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
						break;

					default:
						require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
						break;
				endswitch;

				echo '</li>';
			}
			?>

        </ul>
    </div>

</div>
