<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$id = '';

if ($tagId = $params->get('tag_id', ''))
{
	$id = ' id="' . $tagId . '"';
}

// The menu class is deprecated. Use nav instead
?>


<ul class="menu<?php echo $class_sfx; ?>"<?php echo $id; ?>>
	<?php foreach ($list as $i => &$item)
	{
		$class = 'item-' . $item->id;

		if ($item->id == $default_id)
		{
			$class .= ' default';
		}
		if ($item->id == $active_id || ($item->type === 'alias' && $item->params->get('aliasoptions') == $active_id))
		{
			$class .= ' home';
		}

		if ($item->deeper)
		{
			$class .= ' has-sub';
		}
		echo '<li class="' . $class . '">';

		switch ($item->type) :
			case 'separator':
			case 'component':
			case 'heading':
			case 'url':
				require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
				break;

			default:
				require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
				break;
		endswitch;

		if ($item->deeper)
		{
			echo '<ul class="submenu">';
		}

		// The next item is shallower.
        elseif ($item->shallower)
		{
			echo '</li>';
			echo str_repeat('</ul></li>', $item->level_diff);
		}

		// The next item is on the same level.
		else
		{
			echo '</li>';
		}
	}
	?>
</ul>
