<?php
/**
 * @package     ${NAMESPACE}
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

defined('_JEXEC') or die;

?>
<div class="widget block-main_menu">
    <h5 class="widget-title">
		<?php
		if ($module->showtitle)
		{
			echo $module->title;
		}
		?>
    </h5>
    <ul>

		<?php foreach ($list as $i => &$item)
		{
			$class = 'item-' . $item->id;

			if ($item->id == $default_id)
			{
				$class .= ' default';
			}
			if ($item->id == $active_id || ($item->type === 'alias' && $item->params->get('aliasoptions') == $active_id))
			{
				$class .= ' active';
			}

			echo '<li class="' . $class . '">';

			switch ($item->type) :
				case 'separator':
				case 'component':
				case 'heading':
				case 'url':
					require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
					break;

				default:
					require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
					break;
			endswitch;

			if ($item->deeper)
			{
				echo '<ul class="submenu">';
			}

			// The next item is shallower.
            elseif ($item->shallower)
			{
				echo '</li>';
				echo str_repeat('</ul></li>', $item->level_diff);
			}

			echo '</li>';
		}
		?>

    </ul>
</div>
