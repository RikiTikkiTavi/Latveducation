stdClass Object
(
[id] => 3
[title] => Getting Started (3)
[alias] => getting-started-3
[introtext] => <p>It's easy to get started creating your website. Knowing some of the basics will help.</p>

[fulltext] =>
<h3>What is a Content Management System?</h3>
<p>A content management system is software that allows you to create and manage webpages easily by separating the creation of your content from the mechanics required to present it on the web.</p>
<p>In this site, the content is stored in a <em>database</em>. The look and feel are created by a <em>template</em>. Joomla! brings together the template and your content to create web pages.</p>
<h3>Logging in</h3>
<p>To login to your site use the user name and password that were created as part of the installation process. Once logged-in you will be able to create and edit articles and modify some settings.</p>
<h3>Creating an article</h3>
<p>Once you are logged-in, a new menu will be visible. To create a new article, click on the "Submit Article" link on that menu.</p>
<p>The new article interface gives you a lot of options, but all you need to do is add a title and put something in the content area. To make it easy to find, set the state to published.</p>
<div>You can edit an existing article by clicking on the edit icon (this only displays to users who have the right to edit).</div>
<h3>Template, site settings, and modules</h3>
<p>The look and feel of your site is controlled by a template. You can change the site name, background colour, highlights colour and more by editing the template settings. Click the "Template Settings" in the user menu.</p>
<p>The boxes around the main content of the site are called modules. You can modify modules on the current page by moving your cursor to the module and clicking the edit link. Always be sure to save and close any module you edit.</p>
<p>You can change some site settings such as the site name and description by clicking on the "Site Settings" link.</p>
<p>More advanced options for templates, site settings, modules, and more are available in the site administrator.</p>
<h3>Site and Administrator</h3>
<p>Your site actually has two separate sites. The site (also called the front end) is what visitors to your site will see. The administrator (also called the back end) is only used by people managing your site. You can access the administrator by clicking the "Site Administrator" link on the "User Menu" menu (visible once you login) or by adding /administrator to the end of your domain name. The same user name and password are used for both sites.</p>
<h3>Learn more</h3>
<p>There is much more to learn about how to use Joomla! to create the website you envision. You can learn much more at the <a href="https://docs.joomla.org/" target="_blank" rel="noopener noreferrer">Joomla! documentation site</a> and on the<a href="https://forum.joomla.org/" target="_blank" rel="noopener noreferrer"> Joomla! forums</a>.</p>
[checked_out] => 0
[checked_out_time] => 0000-00-00 00:00:00
[catid] => 2
[created] => 2017-08-24 10:45:49
[created_by] => 715
[created_by_alias] =>
[state] => 1
[modified] => 2017-10-05 19:50:05
[modified_by] => 715
[modified_by_name] => Super User
[publish_up] => 2017-08-24 10:45:49
[publish_down] => 0000-00-00 00:00:00
[images] => {"image_intro":"images\/latveducation_logo.png","float_intro":"none","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}
[urls] => {"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}
[attribs] => {"article_layout":"","show_title":"","link_titles":"","show_tags":"1","show_intro":"0","info_block_position":"","info_block_show_title":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_associations":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_page_title":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}
[metadata] => {"robots":"","author":"","rights":"","xreference":""}
[metakey] =>
[metadesc] =>
[access] => 1
[hits] => 0
[xreference] =>
[featured] => 0
[language] => *
[readmore] => 2757
[category_title] => Uncategorised
[category_route] => uncategorised
[category_access] => 1
[category_alias] => uncategorised
[published] => 1
[parents_published] => 1
[author] => Super User
[author_email] => egor.latvia@gmail.com
[parent_title] => ROOT
[parent_id] => 1
[parent_route] =>
[parent_alias] => root
[rating] => 0
[rating_count] => 0
[alternative_readmore] =>
[layout] =>
[params] => Joomla\Registry\Registry Object
(
[data:protected] => stdClass Object
(
[article_layout] => _:default
[show_title] => 1
[link_titles] => 1
[show_intro] => 0
[show_category] => 1
[link_category] => 1
[show_parent_category] => 0
[link_parent_category] => 0
[show_author] => 1
[link_author] => 0
[show_create_date] => 0
[show_modify_date] => 0
[show_publish_date] => 1
[show_item_navigation] => 1
[show_vote] => 0
[show_readmore] => 1
[show_readmore_title] => 1
[readmore_limit] => 100
[show_icons] => 1
[show_print_icon] => 1
[show_email_icon] => 1
[show_hits] => 1
[show_noauth] => 0
[show_publishing_options] => 1
[show_article_options] => 1
[save_history] => 1
[history_limit] => 10
[show_urls_images_frontend] => 0
[show_urls_images_backend] => 1
[targeta] => 0
[targetb] => 0
[targetc] => 0
[float_intro] => left
[float_fulltext] => left
[category_layout] => _:blog
[show_category_title] => 0
[show_description] => 0
[show_description_image] => 0
[maxLevel] => 1
[show_empty_categories] => 0
[show_no_articles] => 1
[show_subcat_desc] => 1
[show_cat_num_articles] => 0
[show_base_description] => 1
[maxLevelcat] => -1
[show_empty_categories_cat] => 0
[show_subcat_desc_cat] => 1
[show_cat_num_articles_cat] => 1
[num_leading_articles] => 1
[num_intro_articles] => 4
[num_columns] => 2
[num_links] => 4
[multi_column_order] => 0
[show_subcategory_content] => 0
[show_pagination_limit] => 1
[filter_field] => hide
[show_headings] => 1
[list_show_date] => 0
[date_format] =>
[list_show_hits] => 1
[list_show_author] => 1
[orderby_pri] => order
[orderby_sec] => rdate
[order_date] => published
[show_pagination] => 2
[show_pagination_results] => 1
[show_feed_link] => 1
[feed_summary] => 0
[show_page_heading] => 0
[page_title] => Latveducation
[page_description] =>
[page_rights] =>
[robots] =>
[show_tags] => 1
[access-view] => 1
)

[initialized:protected] => 1
[separator] => .
)

[displayDate] => 2017-08-24 10:45:49
[tags] => JHelperTags Object
(
[tagsChanged:protected] =>
[replaceTags:protected] =>
[typeAlias] =>
[itemTags] => Array
(
[0] => stdClass Object
(
[tag_id] => 2
[id] => 2
[parent_id] => 1
[lft] => 1
[rgt] => 2
[level] => 1
[path] => joomla
[title] => Joomla
[alias] => joomla
[note] =>
[description] =>
[published] => 1
[checked_out] => 0
[checked_out_time] => 0000-00-00 00:00:00
[access] => 1
[params] => {"tag_layout":"","tag_link_class":"label label-info","image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}
[metadesc] =>
[metakey] =>
[metadata] => {"author":"","robots":""}
[created_user_id] => 715
[created_time] => 2017-08-24 10:45:49
[created_by_alias] =>
[modified_user_id] => 0
[modified_time] => 0000-00-00 00:00:00
[images] =>
[urls] =>
[hits] => 2
[language] => *
[version] => 1
[publish_up] => 0000-00-00 00:00:00
[publish_down] => 0000-00-00 00:00:00
)

)

)

[slug] => 3:getting-started-3
[parent_slug] =>
[catslug] => 2:uncategorised
[event] => stdClass Object
(
[afterDisplayTitle] =>
[beforeDisplayContent] =>
[afterDisplayContent] =>
)

[text] => <p>It's easy to get started creating your website. Knowing some of the basics will help.</p>

[jcfields] => Array
(
)

)
