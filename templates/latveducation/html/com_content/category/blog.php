<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.caption');

$dispatcher = JEventDispatcher::getInstance();

$this->category->text = $this->category->description;

?>

<div class="flat-row">
    <div class="container">

        <div class="flat-title-section">
            <h1 class="title"><?php echo $this->category->title; ?></h1>
        </div>

        <section class="main-content blog-posts blog-grid have-sidebar main-content-no-padding le-blog">

            <div class="post-content">

                <div class="post-wrap clearfix">

					<?php foreach ($this->items as $key => &$item) :

						$this->item = &$item;
						$this->loadTemplate('item');

						$day    = JFactory::getDate($item->displayDate)->format("d");
						$month  = JFactory::getDate($item->displayDate)->format("M");
						$images = json_decode($item->images);
						$tags   = new JHelperTags;
						$tags->getItemTags('com_content.article', $item->id);
						$link = JRoute::_(ContentHelperRoute::getCategoryRoute($this->item->catslug)).'/'.$item->id;

						?>

                        <article class="post flat-hover-zoom">
                            <div class="featured-post">
                                <div class="entry-image">
                                    <img src="/<?= $images->image_intro ?>" alt="<?= $item->title; ?>">
                                </div>
                            </div>

                            <div class="date-post">
                                <span class="numb">18</span>
                                <span>May</span>
                            </div>

                            <div class="content-post">
                                <h2 class="title-post">
                                    <a href="<?= $link ?>"><?= $item->title; ?></a>
                                </h2>

                                <div class="entry-content">
                                    <p><?= $item->introtext ?></p>
                                </div><!-- /entry-post -->

                                <div class="entry-meta style1">
                                    <p>Posted in:<span><a
                                                    href="<?= JRoute::_(ContentHelperRoute::getCategoryRoute($item->catslug)) ?>"> <?= $item->category_title ?></a></span>
                                    </p>
                                    <p>
                                        Tags:
										<?php foreach ($tags->itemTags as $tag): ?>
                                            <span><a href="<?= JRoute::_(TagsHelperRoute::getTagRoute($tag->tag_id)); ?>"><?= $tag->title ?></a></span>
										<?php endforeach; ?>
                                    </p>
                                </div>
                            </div><!-- /content-post -->
                        </article><!-- /post -->


					<?php endforeach; ?>

                </div><!-- /post-wrap -->

            </div><!-- /row -->
        </section>
    </div><!-- /container -->
</div>