<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
JLoader::register('TagsHelperRoute', JPATH_BASE . '/components/com_tags/helpers/route.php');
// Create shortcuts to some parameters.
$params  = $this->item->params;
$item    = $this->item;
$images  = json_decode($this->item->images);
$urls    = json_decode($this->item->urls);
$canEdit = $params->get('access-edit');
$user    = JFactory::getUser();
$info    = $params->get('info_block_position', 0);
$date    = JFactory::getDate($item->publishDate)->format("y.m.d");

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (JLanguageAssociations::isEnabled() && $params->get('show_associations'));
JHtml::_('behavior.caption');

?>
<div class="flat-row">
    <div class="container">

        <div class="flat-title-section">
            <h1 class="title"><?php echo $item->title; ?></h1>
            <div class="entry-meta">
                <span><i class="fa fa-calendar" aria-hidden="true"></i> <?= $date ?></span>
                <span><i class="fa fa-exclamation-circle" aria-hidden="true"></i> <?= $item->category_title ?></span>
                <span><i class="fa fa-tags" aria-hidden="true"></i>
					<?php foreach ($item -> tags -> itemTags as $tag): ?>
                        <a href="<?= JRoute::_(TagsHelperRoute::getTagRoute($tag->tag_id)); ?>"><?= $tag->title ?></a>,
					<?php endforeach; ?>
                </span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
				<?=$item->fulltext ?>
            </div>
        </div>

    </div>
</div>