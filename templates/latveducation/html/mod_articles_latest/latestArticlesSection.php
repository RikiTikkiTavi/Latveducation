<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('TagsHelperRoute', JPATH_BASE . '/components/com_tags/helpers/route.php');
?>


<div class="row post-lastest-new">
	<?php foreach ($list as $item) :
		$day = JFactory::getDate($item->displayDate)->format("d");
		$month = JFactory::getDate($item->displayDate)->format("M");
		$images = json_decode($item->images);
		$tags = $item->tags->itemTags;
		?>
        <div class="post col-md-4 col-xs-12 col-sm-6 flat-hover-zoom">
            <div class="featured-post">
                <div class="entry-image">
                    <img src="/<?= $images->image_intro ?>" alt="">
                </div>
            </div>

            <div class="date-post">
                <span class="numb"><?= $day ?></span>
                <span><?= $month ?></span>
            </div>

            <div class="content-post">
                <h2 class="title-post">
                    <a href="<?= $item->link; ?>"><?= $item->title; ?></a>
                </h2>

                <div class="entry-content">
                    <p><?= $item->introtext ?></p>
                </div><!-- /entry-post -->

                <div class="entry-meta style1">
                    <p>Posted in:<span><a
                                    href="<?= JRoute::_(ContentHelperRoute::getCategoryRoute($item->catslug)) ?>"> <?= $item->category_title ?></a></span>
                    </p>
                    <p>
                        Tags:
						<?php foreach ($tags as $tag): ?>
                            <span><a href="<?= JRoute::_(TagsHelperRoute::getTagRoute($tag->tag_id)); ?>"><?=$tag->title?></a></span>
						<?php endforeach; ?>
                    </p>
                </div>
            </div><!-- /content-post -->
        </div>
	<?php endforeach; ?>
</div>