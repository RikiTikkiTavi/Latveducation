;(function ($) {
    var ajaxContactForm = function () {
        $('#contactform').each(function () {
            $(this).validate({
                submitHandler: function (form) {
                    var $form = $(form),
                        str = $form.serialize(),
                        loading = $('<div />', {'class': 'loading'});

                    $.ajax({
                        type: "POST",
                        url: formAction,
                        data: str,
                        beforeSend: function () {
                            $form.find('.submit-wrap').append(loading);
                        },
                        success: function (msg) {
                            var result, cls;
                            if (msg === 'Success') {
                                result = successMessage;
                                cls = 'msg-success';
                            } else {
                                result = errorMessage;
                                cls = 'msg-error';
                            }

                            $form.prepend(
                                $('<div />', {
                                    'class': 'flat-alert ' + cls,
                                    'text': result
                                }).append(
                                    $('<a class="close" href="#"><i class="fa fa-close"></i></a>')
                                )
                            );

                            $form.find(':input').not('.submit').val('');
                        },
                        complete: function (xhr, status, error_thrown) {
                            $form.find('.loading').remove();
                        }
                    });
                }
            });
        }); // each contactform
    };

    var ajaxApplyForm = function () {
        $('#applyForm').each(function () {
            $(this).validate({
                submitHandler: function (form) {
                    var $form = $(form),
                        str = new FormData(form),
                        submit_block_html = $form.find('.submit-block').html();

                    $.ajax({
                        type: "POST",
                        url: baseURL + "index.php?option=com_ajax&module=le_latveducation_form&method=sendForm&format=json",
                        data: str,
                        contentType: false,
                        processData: false,
                        beforeSend: function () {
                            $form.find('.submit-block').html('<i style="color:#ffaa30" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>');
                        },
                        success: function (msg) {
                            var result, cls;
                            if (msg.data == 'Success') {
                                result = '<div class="alert alert-success"><p>'+msgSuccess+'</p></div>';
                                cls = 'msg-success';
                            } else {
                                result = '<div class="alert alert-danger"><p>'+msgFailure+'</p></div>';
                                cls = 'msg-error';
                            }

                            $(form).find(".msg_result").html(result);

                            $form.find(':input').not('.submit').val('');

                            $form.find('.submit-block').html(submit_block_html);
                        },
                        complete: function (xhr, status, error_thrown) {
                        }
                    });
                }
            });
        }); // each contactform
    };

    var ajaxSubscribeForm = function () {
        $('#subscribe-form').validate({
            submitHandler: function (form) {
                var email = $(form).find("#subscribe-email").val(),
                    sendTo = $(form).attr('sendTo');

                $.ajax({
                    type: "POST",
                    url: $(form).attr('action'),
                    data: {
                        email: email,
                        sendTo: sendTo
                    },
                    success: function (msg) {
                        var subscribeButton = $("#subscribe-button");
                        if (msg == 'Success') {
                            subscribeButton.addClass("success-subscribe-button");
                            subscribeButton.append(' <i style="color: #3c763d" class="fa fa-check-square" aria-hidden="true"></i>');
                        } else {
                            if (!subscribeButton.find("i").length > 0)
                                $("#subscribe-button").append(' <i style="color: #D64D4B" class="fa fa-exclamation" aria-hidden="true"></i>');
                        }

                        $(form).find('input').val('');

                    }
                });

            }

        })
    };

    var alertBox = function () {
        $(document).on('click', '.close', function (e) {
            $(this).closest('.flat-alert').remove();
            e.preventDefault();
        })
    };

    // Dom Ready
    $(function () {

        ajaxContactForm();
        ajaxSubscribeForm();
        alertBox();
        ajaxApplyForm();

    });

})(jQuery);