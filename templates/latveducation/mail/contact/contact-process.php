<?php

define( '_JEXEC', 1 );

define( 'DS', DIRECTORY_SEPARATOR );
define( 'JPATH_BASE', $_SERVER[ 'DOCUMENT_ROOT' ] );

require_once( JPATH_BASE . DS . 'includes' . DS . 'defines.php' );
require_once( JPATH_BASE . DS . 'includes' . DS . 'framework.php' );
require_once( JPATH_BASE . DS . 'libraries' . DS . 'joomla' . DS . 'factory.php' );

$error  = false;
$fields = array('name', 'email', 'subject', 'message');

foreach ($fields as $field)
{
	if (empty($_POST[$field]) || trim($_POST[$field]) == '')
		$error = true;
}

if (!$error){
	sendFormAjax($fields, $fields);
}

function buildEmailBody($labels, $fields)
{

	$body = "";

	if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");

	foreach ($fields as $i => $field)
	{
		$body .= $labels[$i] . ": " . $_POST[$field] . PHP_EOL;
	}

	return $body;
}

function sendFormAjax($labels, $fields)
{
	$mailer = JFactory::getMailer();

	$mailer->SMTPDebug = true;

	$config = JFactory::getConfig();

	/*Set from whom to send*/
	$sender = array(
		$config->get('mailfrom'),
		$config->get('fromname')
	);
	$mailer->setSender($sender);

	/*TODO: Set reply to*/

	/*To whom*/
	/*$user      = JFactory::getUser();
	$recipient = $user->email;*/
	$mailer->addRecipient($config->get('mailfrom'));

	/*Create mail*/
	$body = buildEmailBody($labels, $fields);
	$mailer->setSubject('Latveducation - Контактная форма: ' . $_POST['subject']);
	$mailer->setBody($body);

	/*Sending email*/
	$send = $mailer->Send();
	if ($send !== true)
	{
		echo 'Error in Mailer';
	}
	else
	{
		echo 'Success';
	}
}

?>