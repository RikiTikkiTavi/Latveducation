<?php

/**
 * @package     Joomla.Site
 * @subpackage  Templates.Latveducation
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

define('_JEXEC', 1);
define('JPATH_BASE', realpath(dirname(__FILE__) . '/../../'));
require_once JPATH_BASE . '/includes/defines.php';
require_once JPATH_BASE . '/includes/framework.php';

$mainframe = JFactory::getApplication('site');

$app = JFactory::getApplication();

/*Get email from template params*/
$tmpl = $app -> getTemplate(true);
$params = $tmpl -> params;
$contactEmail = $params -> get('contactEmail');

$error = false;

function getFields($params){
	if (isset($params)) {
		$form_fields = [];
		$elements = $params ->get('elements', 'Cant get elements');
		foreach ($elements as $element){
			if ($element -> type == "field"){
				array_push($form_fields, $element -> id);
			}
		}
		return $form_fields;
	}
}

/*Get field list*/
$module = JModuleHelper::getModule('mod_le_latveducation_form');
$params = new JRegistry($module->params);

$fields = getFields($params);

if (empty($fields)) $error = true;

if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");

foreach ( $fields as $field ) {
	if ( empty($_POST[$field]) || trim($_POST[$field]) == '' )
		$error = true;
}

if ( !$error ) {

	$name = stripslashes($_POST['name']);
	$email = trim($_POST['email']);
	$subject = stripslashes($_POST['subject']);
	$message = stripslashes($_POST['message']);

	$e_subject = 'You\'ve been contacted by ' . $name . '.';

	// Configuration option.
	// You can change this if you feel that you need to.
	// Developers, you may wish to add more fields to the form, in which case you must be sure to add them here.

	$e_body = "You have been contacted by: $name" . PHP_EOL . PHP_EOL;
	$e_reply = "E-mail: $email" . PHP_EOL . PHP_EOL;
	$e_subject = "\r\nsubject: $subject";
	$e_content = "Message:\r\n$message" . PHP_EOL . PHP_EOL;

	$msg = wordwrap( $e_body . $e_reply .$e_subject , 70 );

	$headers = "From: $email" . PHP_EOL;
	$headers .= "Reply-To: $email" . PHP_EOL;
	$headers .= "MIME-Version: 1.0" . PHP_EOL;
	$headers .= "Content-type: text/plain; charset=utf-8" . PHP_EOL;
	$headers .= "Content-Transfer-Encoding: quoted-printable" . PHP_EOL;

	if(mail($address, $e_subject, $msg, $headers)) {

		// Email has sent successfully, echo a success page.

		echo 'Success';

	} else {

		echo 'ERROR!';

	}

}

?>