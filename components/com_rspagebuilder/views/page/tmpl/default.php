<?php
/**
 * @package RSPageBuilder!
 * @copyright (C) 2016 www.rsjoomla.com
 * @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined ('_JEXEC') or die ('Restricted access');
$rows_padding_page_class = "";
$page_category = "";
if ($this -> page -> rows_padding == '0') $rows_padding_page_class = "page_rows_no_padding";
if (isset($this -> page -> category)) $page_category = $this -> page -> category;
?>



<div id="rspbld" class="rspbld-page<?php echo $this->pageclass_sfx; ?> <?=$rows_padding_page_class?>">
    <script type="text/javascript">
        var page_category = "<?=$page_category?>";
    </script>
	<?php if ($this->show_page_heading) { ?>
	<div class="page-header">
		<h1 itemprop="name">
			<?php
			if ($this->page_heading) {
				echo $this->escape($this->page_heading);
			} else {
				echo $this->page->title;
			}
			?>
		</h1>
	</div>
	<?php } ?>
	<div class="page-content">
		<?php echo ElementParser::viewPage(json_decode($this->page->content), $this->page->bootstrap_version, $this->page->full_width, true); ?>
	</div>
</div>