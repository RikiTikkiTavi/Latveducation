<?php
/**
 * @package       RSPageBuilder!
 * @copyright (C) 2016 www.rsjoomla.com
 * @license       GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined('_JEXEC') or die ('Restricted access');

$app_localvar    = JFactory::getApplication();
$element_options = RSPageBuilderHelper::escapeHtmlArray($displayData['options']);
$items           = $displayData['items'];
$class           = 'rspbld-le-universities-page';
$image_prefix    = (JFactory::getApplication()->isSite()) ? '' : '../';

if ($app_localvar->isClient('administrator'))
{
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"/templates/latveducation/stylesheets/admin/le_universities_page.css\">";
	echo "<div class='leUniPageAdmin'>";
}

/*Build title html*/
if (!empty($element_options['title'])): ?>
    <div class="row">
        <div class="blog-title">
            <h1 class="le-page-title title rspbld-title">
				<?= $element_options['title']; ?>
            </h1>
        </div>
    </div>
<?php endif;

/* Image */
if (!empty($element_options['image'])): ?>
    <div class="row">
        <img class="img-responsive" src="/<?= $element_options['image']; ?>">
    </div>
<?php endif; ?>

<div class="universities_page_description_items">
    <!-- Items -->
	<?php
	if (count($items)):
		foreach ($items as $item):
			$item_options = RSPageBuilderHelper::escapeHtmlArray($item['options']);
			$item_title = $item_options['item_title'];
			$item_description = $item_options['item_description'];
			$item_left_border = $item_options['item_border_show'];
			if ($item_left_border == "1")
			{
				$border_class = "titlebox_leftborder";
			}
			else
			{
				$border_class = "";
			};
			?>

            <div class="row universities_page_description_item">
                <div class="titlebox <?= $border_class ?>">
                    <h5 class="rspbld-title"><?= $item_title ?></h5>
                </div>
                <p><?= $item_description ?></p>
            </div>

			<?php
		endforeach;
	endif; ?>
</div>

<?php
if ($app_localvar->isClient('administrator'))
{
	echo "</div>";
}

?>
