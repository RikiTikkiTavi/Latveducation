<?php
/**
 * @package       RSPageBuilder!
 * @copyright (C) 2016 www.rsjoomla.com
 * @license       GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined('_JEXEC') or die ('Restricted access');

$app_localvar    = JFactory::getApplication();
$element_options = RSPageBuilderHelper::escapeHtmlArray($displayData['options']);
$items           = $displayData['items'];
$class           = 'rspbld-le-projects-section';
$image_prefix    = (JFactory::getApplication()->isSite()) ? '' : '../';

$items_fat  = [];
$items_thin = [];

foreach ($items as $item)
{
	$item_options = RSPageBuilderHelper::escapeHtmlArray($item['options']);
	$is_fat       = $item_options['item_is_fat'];
	if ($is_fat == "0")
	{
		array_push($items_thin, $item);
	}
	else
	{
		array_push($items_fat, $item);
	}
}

if (!empty($element_options['title'])): ?>
    <h1 style="display:none" class="rspbld-title">
		<?= $element_options['title']; ?>
    </h1>
<?php endif; ?>

<div class="row">
	<?php foreach ($items_thin as $item):
		$item_options = RSPageBuilderHelper::escapeHtmlArray($item['options']);
		$image = $item_options['item_image'];
		$is_fat = $item_options['item_is_fat'];
		$description = $item_options['item_description'];
		$menu_item = $item_options['item_menuitem'];
		$link = JRoute::_('index.php?Itemid=' . $menu_item);

		?>


        <div class="col-lg-3">
            <a href="<?= $link ?>"><img height="100px" class="projects_project_img img-hover center-block"
                                         src="/<?= $image ?>"></a>
            <p class="text-center"><?= $description ?></p>
        </div>

	<?php endforeach; ?>
</div>

<div class="row">
    <br>
	<?php foreach ($items_fat as $item):
		$item_options = RSPageBuilderHelper::escapeHtmlArray($item['options']);
		$image = $item_options['item_image'];
		$is_fat = $item_options['item_is_fat'];
		$description = $item_options['item_description'];
		$link = JRoute::_('index.php?Itemid=' . $item_options['item_menuitem']);

		?>


        <div class="col-lg-6">
            <a href="<?=$link?>"><img height="60px" class="projects_project_img-fat img-hover center-block"
                                       src="/<?=$image?>"></a>
            <br>
            <p class="text-center"><?=$description?></p>
        </div>

	<?php endforeach; ?>
</div>