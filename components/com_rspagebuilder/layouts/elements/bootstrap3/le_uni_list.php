<?php
/**
 * @package       RSPageBuilder!
 * @copyright (C) 2016 www.rsjoomla.com
 * @license       GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined('_JEXEC') or die ('Restricted access');

$app_localvar    = JFactory::getApplication();
$element_options = RSPageBuilderHelper::escapeHtmlArray($displayData['options']);
$items           = $displayData['items'];
$class           = 'rspbld-le-uni-list';
$image_prefix    = (JFactory::getApplication()->isSite()) ? '' : '../';
$is_uni          = $element_options['element_type'] == "uni";

if ($app_localvar->isClient('administrator'))
{
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"/templates/latveducation/stylesheets/admin/le_uni_list.css\">";
	echo "<div class='leUniListAdmin'>";
}

/*Build title html*/
if (!empty($element_options['title']) && $is_uni): ?>
    <div class="row universities_page_description_item">
        <div class="titlebox">
            <h5 class="rspbld-title"><?= $element_options['title'] ?></h5>
        </div>
    </div>
<?php endif; ?>

<section class="main-content blog-posts course-list le-course-list">

	<?php if (!empty($element_options['title']) && !$is_uni): ?>
        <div class="blog-title clearfix">
            <h1 class="rspbld-title bold"><?= $element_options['title']; ?></h1>
        </div>
	<?php endif; ?>

    <div class="post-content">


        <div class="post-warp clearfix">

            <!--Items list-->

			<?php
			if (count($items)):
				foreach ($items as $item):
					$item_options = RSPageBuilderHelper::escapeHtmlArray($item['options']);
					$link = JRoute::_('index.php?Itemid=' . $item_options['item_menuitem']);

					?>

                    <div class="flat-course flat-hover-zoom">
                        <div class="featured-post">
                            <div class="overlay">
                                <div class="link"></div>
                            </div>

                            <div class="entry-image">
                                <a href="/<?= $link ?>"><img src="/<?= $item_options['item_image'] ?>"
                                                             alt="<?= $item_options['item_title'] ?>"></a>
                            </div>
                        </div><!-- /.featured-post -->

                        <div class="course-content">
                            <h4><a href="<?= $link ?>"><?= $item_options['item_title'] ?></a></h4>

                            <p> <?= $item_options['item_description'] ?></p>

							<?php if ($is_uni): ?>

                                <ul>
                                    <li>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <span><?= $item_options['item_year_title'] ?></span>
                                        <span class="time"><?= $item_options['item_year'] ?></span>
                                    </li>
                                    <li>
                                        <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                                        <span><?= $item_options['item_type_title'] ?></span>
                                        <span class="time"><?= $item_options['item_type'] ?></span>
                                    </li>
                                </ul>

							<?php endif; ?>

                            <a class="flat-button orange"
                               href="<?= $link ?>"><?= $item_options['item_button_title'] ?></a>
                        </div><!-- /.course-content -->
                    </div>

					<?php
				endforeach;
			endif;
			?>

            <!--/Items list-->

        </div>
    </div>
</section>

<?php
if ($app_localvar->isClient('administrator'))
{
	echo "</div>";
}

?>
