<?php
/**
 * @package       RSPageBuilder!
 * @copyright (C) 2016 www.rsjoomla.com
 * @license       GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined('_JEXEC') or die ('Restricted access');

$app_localvar    = JFactory::getApplication();
$element_options = RSPageBuilderHelper::escapeHtmlArray($displayData['options']);
$class           = 'rspbld-le-news-list';
$image_prefix    = (JFactory::getApplication()->isSite()) ? '' : '../';

/*Build title html*/
if (!empty($element_options['title'])): ?>
	<div class="row universities_page_description_item">
		<div class="titlebox">
			<h5 class="rspbld-title"><?= $element_options['title'] ?></h5>
		</div>
	</div>
<?php endif; ?>

