<?php

// No direct access
defined('_JEXEC') or die ('Restricted access');

$app_localvar = JFactory::getApplication();
$options      = RSPageBuilderHelper::escapeHtmlArray($displayData['options']);
$class        = 'rspbld-le-university-content';

/* Admin styles */
if ($app_localvar->isClient('administrator'))
{
	echo "<link rel='stylesheet' type='text/css' href='/templates/latveducation/stylesheets/admin/le_university_content.css'>";
	echo "<div class='leUniContentAdmin'>";
}

/* If options values is empty */
/*foreach($options as $option => $val){
    if(empty($val)){
        $options[$option] = " ";
    }
}*/

$is_uni_year       = !empty($options['uni_year_title']) && !empty($options['uni_year']);
$is_uni_type       = !empty($options['uni_type_title']) && !empty($options['uni_type']);
$is_uni_location   = !empty($options['uni_location_title']) && !empty($options['uni_location']);
$is_uni_lang       = !empty($options['uni_lang_title']) && !empty($options['uni_lang']);
$is_uni_study_form = !empty($options['uni_study_form_title']) && !empty($options['uni_study_form']);
$is_uni_directions = !empty($options['uni_directions_title']) && !empty($options['uni_directions']);

$title_style = "";
$img_class   = "";

if ($options['element_type'] == "project")
{
	$title_style = "display: none;";
	$img_class   = "img_project";
}

?>

<section class="main-content <?= $class ?> blog-posts course-single">
    <div class="row">
        <div class="blog-title-single">
            <h1 class="rspbld-title bold title" style="<?= $title_style ?>"><?= $options['title'] ?></h1>
            <div class="feature-post ">
                <img src="/<?= $options['image'] ?>" class="img-responsive <?= $img_class ?>" alt="image">
            </div><!-- /.feature-post -->
            <div class="course-widget-price">
                <h4 class="course-title"><?= $options['title'] ?></h4>
                <ul>
					<?php if ($is_uni_year): ?>
                        <li>
                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                            <span><?= $options['uni_year_title'] ?></span>
                            <span class="time"><?= $options['uni_year'] ?></span>
                        </li>
					<?php endif; ?>
					<?php if ($is_uni_type): ?>
                        <li>
                            <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                            <span><?= $options['uni_type_title'] ?></span>
                            <span class="time"><?= $options['uni_type'] ?></span>
                        </li>
					<?php endif; ?>
					<?php if ($is_uni_location): ?>
                        <li>
                            <i class="fa fa-leanpub" aria-hidden="true"></i>
                            <span><?= $options['uni_location_title'] ?></span>
                            <span class="time"><?= $options['uni_location'] ?></span>
                        </li>
					<?php endif; ?>
					<?php if ($is_uni_lang): ?>
                        <li>
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            <span><?= $options['uni_lang_title'] ?></span>
                            <span class="time"><?= $options['uni_lang'] ?></span>
                        </li>
					<?php endif; ?>
					<?php if ($is_uni_study_form): ?>
                        <li>
                            <i class="fa fa-user-plus" aria-hidden="true"></i>
                            <span><?= $options['uni_study_form_title'] ?></span>
                            <span class="time"><?= $options['uni_study_form'] ?></span>
                        </li>
					<?php endif; ?>
					<?php if ($is_uni_directions): ?>
                        <li>
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <span><?= $options['uni_directions_title'] ?></span>
                            <span class="time"><?= $options['uni_directions_form'] ?></span>
                        </li>
					<?php endif; ?>
                </ul>
                <a class="flat-button bg-orange"
                   href="<?= $link = JRoute::_('index.php?Itemid=' . $options['menuitem']); ?>"><?= $options['button_text'] ?></a>
            </div>
            <div class="entry-content content rspbld-content">
                <h4 class="title-1 bold"><?= $options['description_title'] ?></h4>
				<?= $options['content'] ?>
            </div>
        </div>
    </div>
</section>

<?php
/* Admin styles */
if ($app_localvar->isClient('administrator'))
{
	echo "</div>";
}
?>
