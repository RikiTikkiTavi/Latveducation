<?php

// No direct access
defined('_JEXEC') or die ('Restricted access');

$app_localvar = JFactory::getApplication();
$options      = RSPageBuilderHelper::escapeHtmlArray($displayData['options']);
$class        = 'rspbld-le-project-table';

/* Admin styles */
if ($app_localvar->isClient('administrator'))
{
	echo "<link rel='stylesheet' type='text/css' href='/templates/latveducation/stylesheets/admin/le_project_table.css'>";
	echo "<div class='leProjectTableAdmin'>";
}

/* If options values is empty */
foreach ($options as $option => $val)
{
	if (empty($val))
	{
		$options[$option] = " ";
	}
}

?>

<h3 style="display: none" class="rspbld-title title"><?= $options['title'] ?></h3>

<div id="proj-table" class="<?= $class ?>">
    <table align="center">
        <tbody>
        <tr>
            <td><b><span class="masha_index masha_index15" rel="15"></span><?= $options['status_h'] ?></b></td>
            <td><span class="masha_index masha_index16" rel="16"></span><?= $options['status'] ?></td>
        </tr>
        <tr>
            <td><b><span class="masha_index masha_index17" rel="17"></span><?= $options['goal_h'] ?></b></td>
            <td><span class="masha_index masha_index18" rel="18"></span><?= $options['goal'] ?></td>
        </tr>
        <tr>
            <td><b><span class="masha_index masha_index19" rel="19"></span><?= $options['launching_year_h'] ?></b></td>
            <td><span class="masha_index masha_index20" rel="20"></span><?= $options['launching_year'] ?></td>
        </tr>
        <tr>
            <td><b><span class="masha_index masha_index21" rel="21"></span><?= $options['necessary_investments_h'] ?></b></td>
            <td><span class="masha_index masha_index22" rel="22"></span><?= $options['necessary_investments'] ?></td>
        </tr>
        <tr>
            <td><b><span class="masha_index masha_index23" rel="23"></span><?= $options['website_h'] ?></b></td>
            <td><a href="<?= $options['website_link'] ?>"><span class="masha_index masha_index24"
                                                                  rel="24"></span><?= $options['website_link_text'] ?></a>
            </td>
        </tr>
        </tbody>
    </table>
</div>

<?php
/* Admin styles */
if ($app_localvar->isClient('administrator'))
{
	echo "</div>";
}
?>
