<?php

// No direct access
defined('_JEXEC') or die ('Restricted access');

$app_localvar = JFactory::getApplication();
/** @var SplObjectStorage $displayData */
$options = RSPageBuilderHelper::escapeHtmlArray($displayData['options']);
$class   = 'rspbld-le-contacts';

/* Admin styles */
if ($app_localvar->isClient('administrator'))
{
	echo "<link rel='stylesheet' type='text/css' href='/templates/latveducation/stylesheets/admin/le_contacts.css'>";
	echo "<div class='leContactsAdmin'>";
}

/* If options values is empty */
foreach($options as $option => $val){
	if(empty($val)){
		$options[$option] = " ";
	}
}

?>

<div class="contact-content">
	<div class="contact-address">
		<div class="style1">
			<img src="/<?=$options['image']?>" alt="image">
		</div>
		<div class="details">
			<h5 class="rspbld-title title"><?=$options['title']?></h5>
			<div class="rspbld-content"><?=$options['content']?></div>
		</div>
	</div>
</div>

<?php
/* Admin styles */
if ($app_localvar->isClient('administrator'))
{
	echo "</div>";
}
?>
