<?php

// No direct access
defined('_JEXEC') or die ('Restricted access');

$app_localvar = JFactory::getApplication();
/** @var SplObjectStorage $displayData */
$options = RSPageBuilderHelper::escapeHtmlArray($displayData['options']);
$class   = 'rspbld-le-contact-form';

$formAction = JURI::root() . "templates/latveducation/mail/contact/contact-process.php";

/* Admin styles */
if ($app_localvar->isClient('administrator'))
{
	echo "<link rel='stylesheet' type='text/css' href='/templates/latveducation/stylesheets/admin/le_contact_form.css'>";
	echo "<div class='leContactFormAdmin'>";
}

/* If options values is empty */
foreach ($options as $option => $val)
{
	if (empty($val))
	{
		$options[$option] = " ";
	}
}

?>
<div id="respond" class="comment-respond contact style2 <?=$class?>">

    <script type="text/javascript">
        var errorMessage = "<?=$options['alert_error']?>";
        var successMessage = "<?=$options['alert_success']?>";
        var formAction = "<?=$formAction?>";
    </script>

    <h1 class="rspbld-title title comment-title"><?= $options['title'] ?></h1>
    <form id="contactform" class="flat-contact-form style2 bg-dark height-small" method="post">
        <div class="field clearfix">
            <div class="wrap-type-input">
                <div class="input-wrap name">
                    <input type="text" value="" tabindex="1" placeholder="<?= $options['name_placeholder'] ?>"
                           name="name" id="name" required>
                </div>
                <div class="input-wrap email">
                    <input type="email" value="" tabindex="2" placeholder="<?= $options['email_placeholder'] ?>"
                           name="email" id="email" required>
                </div>
                <div class="input-wrap last Subject">
                    <input type="text" value="" placeholder="<?= $options['subject_placeholder'] ?>" name="subject"
                           id="subject">
                </div>
            </div>
            <div class="textarea-wrap">
                <textarea class="type-input" tabindex="3" placeholder="<?= $options['message_placeholder'] ?>"
                          name="message" id="message-contact" required></textarea>
            </div>
        </div>
        <div class="submit-wrap">
            <button class="flat-button bg-orange"><?= $options['button_text'] ?></button>
        </div>
    </form><!-- /.comment-form -->
</div><!-- /#respond -->


<?php
/* Admin styles */
if ($app_localvar->isClient('administrator'))
{
	echo "</div>";
}
?>
