<?php
/**
 * @package       RSPageBuilder!
 * @copyright (C) 2016 www.rsjoomla.com
 * @license       GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined('_JEXEC') or die ('Restricted access');

$app_localvar    = JFactory::getApplication();
$element_options = RSPageBuilderHelper::escapeHtmlArray($displayData['options']);
$items           = $displayData['items'];
$class           = 'rspbld-le-services-section';
$image_prefix    = (JFactory::getApplication()->isSite()) ? '' : '../';

if ($app_localvar->isClient('administrator'))
{
    echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"/templates/latveducation/stylesheets/le_services_section.css\">";
	echo "<div class='leServicesSectionAdmin'>";
}

/*Build title html*/
if (!empty($element_options['title'])): ?>
    <div class="flat-title-section">
        <h1 class="title rspbld-title">
			<?= $element_options['title']; ?>
        </h1>
    </div>
<?php endif;

if (count($items)):
	?>
    <div class="flat-course-grid button-right">

		<?php foreach ($items as $item):
			$item_options = RSPageBuilderHelper::escapeHtmlArray($item['options']);
			$link = JRoute::_('index.php?Itemid=' . $item_options['item_menuitem']);
			$sc1 = !empty($item_options['item_sect1_title']) && !empty($item_options['item_sect1_content']);
			$sc2 = !empty($item_options['item_sect2_title']) && !empty($item_options['item_sect2_content']);
			$sc3 = !empty($item_options['item_sect3_title']) && !empty($item_options['item_sect3_content']);
			?>

            <div class="flat-course">
                <div class="featured-post">
                    <div class="overlay">
                        <div class="link"></div>
                    </div>

                    <a href="<?= $link ?>"><img src="/<?= $item_options['item_image']; ?>" alt="Course1"></a>
                </div><!-- /.featured-post -->

                <div class="course-content">
                    <h4 class="rspbld-title"><a href="<?= $link ?>"><?= $item_options['item_title'] ?></a></h4>

                    <div class="price"><?= $item_options['item_price']; ?></div>

                    <!--<ul class="course-meta review">
                        <li class="review-stars">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half"></i>
                            <i class="fa fa-star-o"></i>
                        </li>

                        <li>25 Reviews</li>
                    </ul>-->

                    <p> <?= $item_options['item_description'] ?></p>

                    <ul class="course-meta desc">
						<?php if ($sc1): ?>
                            <li>
                                <h6><?= $item_options['item_sect1_content']; ?></h6>
                                <span class="rspbld-title"> <?= $item_options['item_sect1_title']; ?></span>
                            </li>
						<?php endif; ?>
						<?php if ($sc2): ?>
                            <li>
                                <h6><?= $item_options['item_sect2_content']; ?></h6>
                                <span class="rspbld-title"> <?= $item_options['item_sect2_title']; ?></span>
                            </li>
						<?php endif; ?>
						<?php if ($sc3): ?>
                            <li>
                                <h6><?= $item_options['item_sect3_content']; ?></h6>
                                <span class="rspbld-title"> <?= $item_options['item_sect3_title']; ?></span>
                            </li>
						<?php endif; ?>
                    </ul>
                </div><!-- /.course-content -->
            </div>

		<?php endforeach; ?>

    </div>

<?php endif;

if ($app_localvar->isClient('administrator'))
{
	echo "</div>";
}

?>
