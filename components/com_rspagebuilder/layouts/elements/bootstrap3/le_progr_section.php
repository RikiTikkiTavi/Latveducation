<?php

// No direct access
defined('_JEXEC') or die ('Restricted access');

$app_localvar = JFactory::getApplication();
/** @var SplObjectStorage $displayData */
$options = RSPageBuilderHelper::escapeHtmlArray($displayData['options']);
$items   = $displayData['items'];
$class   = 'rspbld-le-progr-section';

/* Admin styles */
if ($app_localvar->isClient('administrator'))
{
	echo "<link rel='stylesheet' type='text/css' href='/templates/latveducation/stylesheets/admin/le_progr_section.css'>";
	echo "<div class='leProgrSectionAdmin'>";
}
?>

<h4 class="rspbld-title bold"><?= $options['title'] ?></h4>
<div class="table-responsive <?=$class?>">
    <table class="table">
        <tr>
            <td></td>
            <td><?= $options['column_1'] ?></td>
            <td><?= $options['column_2'] ?></td>
            <td><?= $options['column_3'] ?></td>
            <td><?= $options['column_4'] ?></td>
            <td><?= $options['column_5'] ?></td>
        </tr>

		<?php
		foreach ($items as $item):
			$item_options = RSPageBuilderHelper::escapeHtmlArray($item['options']);
			?>
            <tr>
                <td><?= $item_options['item_column_0'] ?></td>
                <td><?= $item_options['item_column_1'] ?></td>
                <td><?= $item_options['item_column_2'] ?></td>
                <td><?= $item_options['item_column_3'] ?></td>
                <td><?= $item_options['item_column_4'] ?></td>
                <td><?= $item_options['item_column_5'] ?></td>
            </tr>

			<?php
		endforeach;
		?>
    </table>
</div>

<?php
/* Admin styles */
if ($app_localvar->isClient('administrator'))
{
	echo "</div>";
}
?>
