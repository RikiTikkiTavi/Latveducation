<?php
/**
 * @package       RSPageBuilder!
 * @copyright (C) 2016 www.rsjoomla.com
 * @license       GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined('_JEXEC') or die ('Restricted access');

$app_localvar    = JFactory::getApplication();
$element_options = RSPageBuilderHelper::escapeHtmlArray($displayData['options']);
$items           = $displayData['items'];
$class           = 'rspbld-projects-column';
$button_class    = 'rspbld-button btn';
$title_show      = (!isset($element_options['title_show']) || (isset($element_options['title_show']) && ($element_options['title_show'] == '1'))) ? '1' : '0';
$style           = array();
$image_prefix    = (JFactory::getApplication()->isSite()) ? '' : '../';

// Projects Column image
if (!empty($element_options['image']))
{
	$element_options['image'] = ' src="' . $image_prefix . $element_options['image'] . '"';

	if (!empty($element_options['image_alt_text']))
	{
		$element_options['image_alt_text'] = ' alt="' . $element_options['image_alt_text'] . '"';
	}

}

if ($app_localvar->isClient('administrator'))
{
	echo "<div class='projectsColumnAdmin'>";
}

if (!empty($element_options['title']))
{
	?>
	<?php
	if (!empty($element_options['title']))
	{
		// Build Project Column title HTML
		?>
        <div class="row text-center">
            <div class="col-md-12">
                <<?php echo $element_options['title_heading']; ?> class="rspbld-title">
				<?php echo $element_options['title']; ?>
            </<?php echo $element_options['title_heading']; ?>>
            </div>
        </div>
		<?php
	}
	?>
<?php } ?>

<?php
if (count($items))
{
	foreach ($items as $item)
	{
		$item_options = RSPageBuilderHelper::escapeHtmlArray($item['options']);

		// Build Project Column item HTML
		?>
        <div class="row">
            <div class="col-md-12">
                <img class="img-responsive" src="/<?php echo $item_options['item_image']; ?>"
                     alt="<?php echo $item_options['item_image_alt_text']; ?>">
            </div>
        </div>

		<?php
	}
}

if ($app_localvar->isClient('administrator'))
{
	echo "</div>";
}
?>
