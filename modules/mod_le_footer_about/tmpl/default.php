<?php
// No direct access
defined('_JEXEC') or die;

/** @var object $fields */
?>

<div class="widget block-about">
    <h5 class="widget-title"><?=$fields->heading?></h5>
    <p><img width="50%" class="img-responsive center-block" src="/<?=$fields->logo?>"></p>
    <div class="row">
        <div class="col-md-1 col-sm-1 col-xs-1"><i class="fa fa-map-marker" style="font-size: 1.3rem" aria-hidden="true"></i></div>
        <div class="col-md-10 col-sm-10 col-xs-10"><p style="margin-left: 15px;"><?=$fields->address?></p></div>
    </div>

    <div class="row">
        <div class="col-md-1 col-sm-1 col-xs-1"><i style="transform: rotate(-60grad); font-size: 1.3rem; margin-top: 7px" class="fa fa-credit-card" aria-hidden="true"></i></div>
        <div class="col-md-10 col-sm-10 col-xs-10"><p style="margin-left: 15px;"><?=$fields->bank_account?></p></div>
    </div>

    <div class="row">
        <div style="margin-bottom: 5px;" class="col-md-12 text-center "><span class="text-center"><?=$fields->payments_text?></span></div>
        <div class="col-md-6 col-sm-6 col-xs-6 text-right"><i class="fa fa-cc-visa fa-3x" aria-hidden="true"></i></div>
        <div class="col-md-6 col-sm-6 col-xs-6 text-left"><i class="fa fa-cc-paypal fa-3x" aria-hidden="true"></i></div>
    </div>

</div>
