<?php
// No direct access
defined('_JEXEC') or die;

/** @var object $fields */
?>

<div class="widget block-contacts">
    <h5 class="widget-title"><?= $fields->heading ?></h5>

    <div class="row">

        <!--Left col-->
        <div class="col-md-6 col-sm-6">

            <!--LV-->
            <h5 class="sub-heading"><?= $fields->sub_heading_lv ?></h5>

            <!--Phone-->
            <div class="media">
                <div class="media-h">
					<?= $fields->phone_lv_heading ?>
                </div>
                <span class="pull-left">
                    <i class="fa fa-phone media-object" aria-hidden="true"></i>
                </span>
                <div class="media-body">
					<?= $fields->phone_lv ?>
                </div>
            </div>

            <!--Mail-->
            <div class="media">
                <div class="media-h">
					<?= $fields->mail_lv_heading ?>
                </div>
                <span class="pull-left">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                </span>
                <div class="media-body">
					<?= $fields->mail_lv ?>
                </div>
            </div>

            <!--Social-->
            <div class="media footer-social">
                <div class="media-h"><?= $fields->social_heading ?></div>
                <div class="row">
                    <div class="col-sm-3 col-xs-3"><a href="<?= $fields->social_1 ?>"><i class="fa fa-linkedin-square fa-2x"
                                                                                aria-hidden="true"></i></a></div>
                    <div class="col-sm-3 col-xs-3"><a href="<?= $fields->twitter ?>"><i class="fa fa-twitter fa-2x"
                                                                               aria-hidden="true"></i></a></div>
                    <div class="col-sm-3 col-xs-3"><a href="<?= $fields->facebook ?>"><i class="fa fa-facebook-official fa-2x"
                                                                                aria-hidden="true"></i></a></div>
                    <div class="col-sm-3 col-xs-3"><a href="<?= $fields->youtube ?>"><i class="fa fa-youtube-play fa-2x"
                                                                               aria-hidden="true"></i></a></div>
                </div>
            </div>
        </div>

        <!--Right col-->
        <div class="col-md-6 col-sm-6">

            <!--RU-->

            <div class="sub-block-ru">
                <h5 class="sub-heading"><?= $fields->sub_heading_ru ?></h5>

                <!--Phone-->
                <div class="media">
                <span class="pull-left">
                    <i class="fa fa-phone media-object" aria-hidden="true"></i>
                </span>
                    <div class="media-body">
						<?= $fields->phone_lv ?>
                    </div>
                </div>

                <!--Mail-->
                <div class="media">
                <span class="pull-left">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                </span>
                    <div class="media-body">
						<?= $fields->mail_lv ?>
                    </div>
                </div>
            </div>

            <!--UA-->
            <div class="sub-block-ua">
                <h5 class="sub-heading"><?= $fields->sub_heading_ua ?></h5>

                <!--Phone-->
                <div class="media">
                <span class="pull-left">
                    <i class="fa fa-phone media-object" aria-hidden="true"></i>
                </span>
                    <div class="media-body">
						<?= $fields->phone_ua ?>
                    </div>
                </div>

                <!--Mail-->
                <div class="media">
                <span class="pull-left">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                </span>
                    <div class="media-body">
						<?= $fields->mail_ua ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
