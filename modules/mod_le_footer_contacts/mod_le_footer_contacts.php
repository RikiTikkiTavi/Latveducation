<?php
/**
 * LE_SLIDER entry point
 *
 * @package Joomla.Site
 * @subpackage Modules
 * @license Payment
 * @copyright Egor
 * @since 14.09.2017
 */

defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';

//Services arr
if (isset($params)) {
   $fields = $params ->get('fields_block_fields', "Can't get fields");
}


require JModuleHelper::getLayoutPath('mod_le_footer_contacts', $params->get('layoutx', 'default'));
