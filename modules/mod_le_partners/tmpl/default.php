<?php
// No direct access
defined('_JEXEC') or die;
?>

<ul class="partners-slider">

	<?php foreach ($partners as $partner): ?>

        <li>
            <img alt="<?= $partner->p_name ?>" src="/<?=$partner->p_logo?>"/>
        </li>

	<?php endforeach; ?>

</ul>