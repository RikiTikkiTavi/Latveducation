<?php
/**
 * LEP_PORTFOLIO entry point
 *
 * @package Joomla.Site
 * @subpackage Modules
 * @license Payment
 * @copyright Egor
 * @since 07.05.2017
 */

defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';

//Services arr
if (isset($params)) {
    $partners = $params ->get('partners', 'Error in partners module');
}


require JModuleHelper::getLayoutPath('mod_le_partners');
