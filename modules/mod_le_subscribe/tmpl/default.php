<?php
// No direct access
defined('_JEXEC') or die;
?>

<div class="have-question">
    <h2 class=""><?=$have_question_heading?></h2>

    <p class="flat-lh-28">
	    <?=$have_question_text?>
    </p>

    <div class="flat-button-container">
        <a class="read-more" href="<?=$have_question_menuitem_link?>"><?=$have_question_link_text?></a>
    </div>
</div>

<div class="news-letter-form">
    <div class="widget-mailchimb">
        <h1 class="widget-title"><?=$news_letter_form_heading?></h1>
        <p><?=$news_letter_form_text?></p>
        <form method="post" action="" sendTo="<?=$email?>" id="subscribe-form" data-mailchimp="true">
            <div id="subscribe-content">
                <div class="input-wrap email">
                    <input type="email" id="subscribe-email" name="subscribe-email"
                           placeholder="<?=$news_letter_form_input_text?>">
                </div>
                <div class="button-wrap">
                    <button type="submit" id="subscribe-button" class="subscribe-button"
                            title="Subscribe now"> <?=$news_letter_form_button_text?>
                    </button>
                </div>
            </div>
            <div id="subscribe-msg">
                <!--<div id="subscribe-msg-fail"><?/*=$news_letter_form_fail_message*/?></div>-->
            </div>
        </form>

    </div>
</div>