<?php
/**
 * LEP_PORTFOLIO entry point
 *
 * @package Joomla.Site
 * @subpackage Modules
 * @license Payment
 * @copyright Egor
 * @since 07.05.2017
 */

defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';


if (isset($params)) {
    $email = $params ->get('email', 'Error in subscribe module');

	$have_question_heading = $params ->get('have_question_heading', 'Error in subscribe module');
    $have_question_text = $params ->get('have_question_text', 'Error in subscribe module');

    $news_letter_form_heading = $params ->get('news_letter_form_heading', 'Error in subscribe module');
    $news_letter_form_text = $params ->get('news_letter_form_text', 'Error in subscribe module');
    $news_letter_form_input_text = $params ->get('news_letter_form_input_text', 'Error in subscribe module');
	$news_letter_form_button_text = $params ->get('news_letter_form_button_text', 'Error in subscribe module');

	$news_letter_form_success_message = $params ->get('news_letter_form_succes_message', 'Error in subscribe module');
	$news_letter_form_fail_message = $params ->get('news_letter_form_fail_message', 'Error in subscribe module');
}


require JModuleHelper::getLayoutPath('mod_le_subscribe');
