<?php
// No direct access
defined('_JEXEC') or die;
?>

<!-- Slider -->
<div class="tp-banner-container">
    <div class="tp-banner">
        <ul>

			<?php if (isset($slides))
			{
				foreach ($slides as $slide)
				{
					?>

                    <!-- Slide -->

                    <li class="slider_li" data-transition="random-static" data-slotamount="7" data-masterspeed="1000"
                        data-saveperformance="on">
                        <img src="<?= $slide->slide_img ?>" alt=""/>
                        <div class="li_overlay"></div>
						<?php if (isset($slide->slide_heading)): ?>
                            <div class="tp-caption sfl title-slide center" data-x="174" data-y="250"
                                 data-speed="1000"
                                 data-start="1000" data-easing="Power3.easeInOut">
								<?= $slide->slide_heading ?>
                            </div>
						<?php endif; ?>

						<?php if (isset($slide->slide_text)): ?>
                            <div class="tp-caption sfr desc-slide center" data-x="256" data-y="310" data-speed="1000"
                                 data-start="1500" data-easing="Power3.easeInOut">
								<?= $slide->slide_text ?>
                            </div>
						<?php endif; ?>

						<?php if (isset($slide->slide_button_text_1)): ?>
                            <div class="tp-caption sfl flat-button-slider bg-orange" data-x="420" data-y="439"
                                 data-speed="1000"
                                 data-start="2000" data-easing="Power3.easeInOut"><a class=""
                                                                                     href="<?= JRoute::_('index.php?Itemid=' . $slide->slide_menuitem_1) ?>"><?= $slide->slide_button_text_1 ?></a>
                            </div>
						<?php endif; ?>
						<?php if (isset($slide->slide_button_text_2)): ?>
                            <div class="tp-caption sfr flat-button-slider" data-x="601" data-y="440"
                                 data-speed="1000"
                                 data-start="2500" data-easing="Power3.easeInOut"><a class=""
                                                                                     href="<?= JRoute::_('index.php?Itemid=' . $slide->slide_menuitem_2) ?>"><?= $slide->slide_button_text_2 ?></a>
                            </div>
						<?php endif; ?>

                    </li>
                    <!-- /Slide -->

				<?php } /* foreach */
			} /* if(isset()) */
			?>

        </ul>
    </div>
</div>
<!-- /.tp-banner-container -->
