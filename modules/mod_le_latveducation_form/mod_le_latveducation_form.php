<?php
/**
 * @copyright	@copyright	Copyright (c) 2017 le_latveducation_form. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

// include the syndicate functions only once
require_once __DIR__ . '/helper.php';

$app    = JFactory::getApplication();
$template_url   = JURI::base(true).'/templates/'.$app->getTemplate().'/';
$baseURL = JURI::root();



if (isset($params)) {
	$msg_success = $params -> get('success_message', 'Cant get success message');
	$msg_failure = $params -> get('failure_message', 'Cant get failure message');
	$elements = $params ->get('elements', 'Cant get elements');
}


/** @noinspection PhpIncludeInspection */
require JModuleHelper::getLayoutPath('mod_le_latveducation_form');