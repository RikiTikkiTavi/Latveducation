<?php
/**
 * @copyright      Copyright (c) 2017 Latveducation. All rights reserved.
 * @license        http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

function is_option($element)
{
	$type      = $element->element_type;
	$is_option = $type == 'option';

	return $is_option;
}

function display_options($i, $elements)
{
	while (true)
	{
		$i++;

		$element = $elements->$i;
		if (is_option($element))
		{
			echo '<option value="' . $element->option_value . '">' . $element->option_text . '</option>';
		}
		else
		{
			break;
		}
	}
}

?>

<script>
    var baseURL = "<?=$baseURL?>";
    var msgSuccess = "<?=/** @var string $msg_success */
		$msg_success?>";
    var msgFailure = "<?=/** @var string $msg_failure */
		$msg_failure?>";
</script>

<form class="form-horizontal" role="form" id="applyForm" enctype="multipart/form-data">

	<?php
	/** @var stdClass $elements */
	foreach ($elements as $i => $element):

		$type = $element->element_type;

		$is_divider = $type == 'divider';
		$is_field   = $type == 'field';
		$is_option  = $type == 'option';

		$is_select = $element->type == 'select';

		?>


        <!--Divider-->
		<?php if ($is_divider): ?>
        <div class="form-group">
            <h3><?= $element->block_title ?></h3>
            <hr>
        </div>

        <!--Input field-->
	<?php elseif ($is_field && !$is_select): ?>
        <div class="form-group">
            <label for="<?= $element->id; ?>" class="col-md-1 control-label"><?= $element->label ?>:</label>
            <div class="col-md-11">
                <input required type="<?= $element->type ?>" class="form-control" id="<?= $element->id ?>"
                       name="<?= $element->id ?>" placeholder="<?= $element->placeholder ?>">
            </div>
        </div>

        <!--Select field-->
	<?php elseif ($is_field && $is_select): ?>
        <div class="form-group">
            <label for="<?= $element->id; ?>" class="col-md-1 control-label"><?= $element->label ?>:</label>
            <div class="col-md-11">
                <select class="form-control" id="<?= $element->id; ?>" name="<?= $element->id ?>">
					<?php display_options($i, $elements) ?>
                </select>
            </div>
        </div>

	<?php endif; ?>

	<?php endforeach; ?>

    <div class="form-group text-center">
        <div class="submit-block">
            <button class="flat-button bg-orange">Send</button>
        </div>
    </div>

    <div class="form-group text-center msg_result">
        <p></p>
    </div>

</form>
