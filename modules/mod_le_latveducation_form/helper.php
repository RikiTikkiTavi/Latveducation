<?php
/**
 * @copyright      Copyright (c) 2017 le_latveducation_form. All rights reserved.
 * @license        http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

/**
 * le_latveducation_form - Latveducation Form Helper Class.
 *
 * @package        Joomla.Site
 * @subpakage      le_latveducation_form.LatveducationForm
 */
class modLeLatveducationFormHelper
{

	public static function getFields()
	{

		$module = JModuleHelper::getModule('mod_le_latveducation_form');
		$params = new JRegistry($module->params);
		$goi    = "goi";

		if (isset($params))
		{
			$form_fields = [];
			$form_labels = [];
			$form_files = [];
			$elements    = $params->get('elements', 'Cant get elements');
			foreach ($elements as $element)
			{
				if ($element->element_type == "field" && $element->type != "file")
				{
					array_push($form_fields, $element->id);
					array_push($form_labels, $element ->label);
				}
				if ($element->type == "file"){
					array_push($form_files, $element ->id);
				}
			}

			$form_fields_and_labels_arrs = [];

			array_push($form_fields_and_labels_arrs, $form_fields);
			array_push($form_fields_and_labels_arrs, $form_labels);
			array_push($form_fields_and_labels_arrs, $form_files);

			return $form_fields_and_labels_arrs;
		}
	}

	public static function buildEmailBody($labels, $fields){

		$body = "";

		if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");

		foreach ($fields as $i => $field){
			$body .= $labels[$i].": ". $_POST[$field] . PHP_EOL;
		}

		return $body;
	}

	public static function uploadFilesToJoomla($files){

		jimport('joomla.filesystem.file');

		$destinations = [];

		foreach ($files as $file_id){

			$file = $_FILES[$file_id];
			$filename = JFile::makeSafe($file['name']);

			// Set up the source and destination of the file
			$src = $file['tmp_name'];
			$dest = JPATH_BASE . "/images/user_uploads/" . $filename;

			JFile::upload($src, $dest);

			array_push($destinations, $dest);

		}

		return $destinations;

	}

	public static function sendFormAjax()
	{
		$app = JFactory::getApplication();

		$error = false;

		$form_fields_and_labels_arrs = modLeLatveducationFormHelper::getFields();
		$fields = $form_fields_and_labels_arrs[0];
		$labels = $form_fields_and_labels_arrs[1];
		$files =  $form_fields_and_labels_arrs[2];

		if (empty($fields)) $error = true;

		foreach ($fields as $field)
		{
			if (empty($_POST[$field]) || trim($_POST[$field]) == '')
				$error = true;
		}


		/*Build message*/
		if (!$error)
		{

			$mailer = JFactory::getMailer();

			$mailer->SMTPDebug = true;

			$config = JFactory::getConfig();

			/*Set from whom to send*/
			$sender = array(
				$config->get('mailfrom'),
				$config->get('fromname')
			);
			$mailer->setSender($sender);

			/*TODO: Set reply to*/

			/*To whom*/
			/*$user      = JFactory::getUser();
			$recipient = $user->email;*/
			$mailer->addRecipient($config->get('mailfrom'));

			/*Create mail*/
			$body = modLeLatveducationFormHelper::buildEmailBody($labels, $fields);
			$mailer->setSubject('Latveducation - Заявка');
			$mailer->setBody($body);

			//File attached
			$destinations = modLeLatveducationFormHelper::uploadFilesToJoomla($files);
			foreach ($destinations as $dest){
				$mailer->addAttachment($dest);
			}

			/*Sending email*/
			$send = $mailer->Send();
			if ( $send !== true ) {
				return 'Error in Mailer';
			} else {
				return 'Success';
			}
		}
		else{
			return 'Error';
		}
	}
}