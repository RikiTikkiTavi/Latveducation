<?php
/**
 * @package RSPageBuilder!
 * @copyright (C) 2016 www.rsjoomla.com
 * @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined ('_JEXEC') or die ('Restricted access');

use Joomla\Utilities\ArrayHelper;

class RSPageBuilderHelper {

	/**
	 * Retrieve a list of article
	 *
	 * @param   \Joomla\Registry\Registry  &$params  module parameters
	 *
	 * @return  mixed
	 *
	 * @since   1.6
	 */
	public static function getListOfLatestArticles(&$params)
	{

		JLoader::register('ContentHelperRoute', JPATH_SITE . '/components/com_content/helpers/route.php');

		JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_content/models', 'ContentModel');

		// Get the dbo
		$db = JFactory::getDbo();

		// Get an instance of the generic articles model
		$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));

		// Set application parameters in model
		$app       = JFactory::getApplication();
		$appParams = $app->getParams();
		$model->setState('params', $appParams);

		// Set the filters based on the module params
		$model->setState('list.start', 0);
		$model->setState('list.limit', (int) $params->get('count', 5));
		$model->setState('filter.published', 1);

		// This module does not use tags data
		$model->setState('load_tags', true);

		// Access filter
		$access     = !JComponentHelper::getParams('com_content')->get('show_noauth');
		$authorised = JAccess::getAuthorisedViewLevels(JFactory::getUser()->get('id'));
		$model->setState('filter.access', $access);

		// Category filter
		$model->setState('filter.category_id', $params->get('catid', array()));

		// User filter
		$userId = JFactory::getUser()->get('id');

		switch ($params->get('user_id'))
		{
			case 'by_me' :
				$model->setState('filter.author_id', (int) $userId);
				break;
			case 'not_me' :
				$model->setState('filter.author_id', $userId);
				$model->setState('filter.author_id.include', false);
				break;

			case '0' :
				break;

			default:
				$model->setState('filter.author_id', (int) $params->get('user_id'));
				break;
		}

		// Filter by language
		$model->setState('filter.language', $app->getLanguageFilter());

		//  Featured switch
		switch ($params->get('show_featured'))
		{
			case '1' :
				$model->setState('filter.featured', 'only');
				break;
			case '0' :
				$model->setState('filter.featured', 'hide');
				break;
			default :
				$model->setState('filter.featured', 'show');
				break;
		}

		// Set ordering
		$order_map = array(
			'm_dsc' => 'a.modified DESC, a.created',
			'mc_dsc' => 'CASE WHEN (a.modified = ' . $db->quote($db->getNullDate()) . ') THEN a.created ELSE a.modified END',
			'c_dsc' => 'a.created',
			'p_dsc' => 'a.publish_up',
			'random' => $db->getQuery(true)->Rand(),
		);

		$ordering = ArrayHelper::getValue($order_map, $params->get('ordering'), 'a.publish_up');
		$dir      = 'DESC';

		$model->setState('list.ordering', $ordering);
		$model->setState('list.direction', $dir);

		$items = $model->getItems();

		foreach ($items as &$item)
		{
			$item->slug    = $item->id . ':' . $item->alias;

			/** @deprecated Catslug is deprecated, use catid instead. 4.0 **/
			$item->catslug = $item->catid . ':' . $item->category_alias;

			if ($access || in_array($item->access, $authorised))
			{
				// We know that user has the privilege to view the article
				$item->link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catid, $item->language));
			}
			else
			{
				$item->link = JRoute::_('index.php?option=com_users&view=login');
			}
		}

		return $items;
	}

	public static function loadAsset($type, $file) {
		$ext = JFile::getExt($file);
		switch (true) {
			case ($type == 'component' and $ext == 'css'):
				JHtml::_('stylesheet', 'com_rspagebuilder/'.$file, array(), true);
			break;
			
			case ($type == 'element' and $ext == 'css'):
				JHtml::_('stylesheet', 'com_rspagebuilder/elements/'.$file, array(), true);
			break;
			
			case ($type == 'module' and $ext == 'css'):
				JHtml::_('stylesheet', 'mod_rspagebuilder_elements/'.$file, array(), true);
			break;
			
			case ($type == 'component' and $ext == 'js'):
				JHtml::_('script', 'com_rspagebuilder/'.$file, false, true);
			break;
			
			case ($type == 'element' and $ext == 'js'):
				JHtml::_('script', 'com_rspagebuilder/elements/'.$file, array(), true);
			break;
			
			case ($type == 'module' and $ext == 'js'):
				JHtml::_('script', 'mod_rspagebuilder_elements/'.$file, false, true);
			break;
		}
	}
	
	public static function loadElementLayout($element, $bootstrap_version) {
		if (JFactory::getApplication()->isSite()) {
			$layout = new JLayoutFile('elements.bootstrap'.$bootstrap_version.'.'.str_replace('rspbld_', '', $element['type']), null, array('component' => 'com_rspagebuilder'));
		} else {
			if (JFile::exists(JPATH_ROOT . '/templates/'.self::getTemplate().'/html/layouts/com_rspagebuilder/elements/bootstrap'.$bootstrap_version.'/'.str_replace('rspbld_', '', $element['type']).'.php')) {
				$layout = new JLayoutFile(str_replace('rspbld_', '', $element['type']), JPATH_ROOT . '/templates/'.self::getTemplate().'/html/layouts/com_rspagebuilder/elements/bootstrap'.$bootstrap_version);
			} else if (JFile::exists(JPATH_ROOT . '/components/com_rspagebuilder/layouts/elements/bootstrap'.$bootstrap_version.'/'.str_replace('rspbld_', '', $element['type']).'.php')) {
				$layout = new JLayoutFile(str_replace('rspbld_', '', $element['type']), JPATH_ROOT . '/components/com_rspagebuilder/layouts/elements/bootstrap'.$bootstrap_version);
			}
		}
		
		return $layout->render($element);
	}
	
	public static function getElementIcon($element_type) {
		$element_type 			= str_replace('rspbld_', '', $element_type);
		$icon_template_path		= JPATH_ROOT . '/templates/' . self::getTemplate() . '/images/com_rspagebuilder/icons/' . $element_type . '.png';
		$icon_component_path	= JPATH_ROOT . '/media/com_rspagebuilder/images/icons/' . $element_type . '.png';
		
		if (file_exists($icon_template_path)) {
			$icon = JUri::root(true) . '/templates/' . self::getTemplate() . '/images/com_rspagebuilder/icons/' . $element_type . '.png';
		} else if (file_exists($icon_component_path)) {
			$icon = JUri::root(true) . '/media/com_rspagebuilder/images/icons/' . $element_type . '.png';
		} else {
			$icon = JUri::root(true) . '/media/com_rspagebuilder/images/icons/default.png';
		}
		
		return $icon;
	}
	
	private static function getTemplate() {
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true)
			->select($db->qn(array('template')))
			->from($db->qn('#__template_styles'))
			->where($db->qn('client_id') . ' = 0')
			->where($db->qn('home') . ' = 1');
		$db->setQuery($query);

		return $db->loadObject()->template;
	}
	
	public static function loadGoogleFonts() {
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true)
			->select($db->qn('params'))
			->from($db->qn('#__template_styles'))
			->where($db->qn('client_id') . ' = 0')
			->where($db->qn('home') . ' = 1');
		$db->setQuery($query);
		
		$doc				= JFactory::getDocument();
		$params				= json_decode($db->loadObject()->params);
		$uri             	= JURI::getInstance();
		$prefix_protocol	= 'http';
		$sufix				= '';
		$fonts				= array();
		
		if ($uri->isSSL()) {
			$prefix_protocol .= 's';
		}
		
		if (!empty($params->googleTitleFont)) {
			$fonts['title'] = $params->googleTitleFont;
		}
		
		if (!empty($params->googleContentFont)) {
			$fonts['content'] = $params->googleContentFont;
		}
		
		if (!empty($params->googleNavbarFont)) {
			$fonts['navbar'] = $params->googleNavbarFont;
		}
		
		if (!empty($params->googleFontSubset)) {
			$sufix = '&amp;subset=' . $params->googleFontSubset;
		}
		
		$fonts = array_unique($fonts);
		
		foreach ($fonts as $font) {
			$doc->addCustomTag('<link href="' . $prefix_protocol . '://fonts.googleapis.com/css?family=' . $font . $sufix . '" rel="stylesheet" type="text/css" />');
		}
		
		// Title selectors with Google Font
		if (!empty($params->addGoogleTitleClasses)) {
			$extraTitleSelectors = trim($params->addGoogleTitleClasses, ',');
			$doc->addStyleDeclaration(
				$extraTitleSelectors . "{
					font-family: " . current(explode(":", $params->googleTitleFont)) . "
				}"
			);
		}
		
		// Content selectors with Google Font
		if (!empty($params->addGoogleContentClasses)) {
			$extraContentSelectors = trim($params->addGoogleContentClasses, ',');
			$doc->addStyleDeclaration(
				$extraContentSelectors . "{
					font-family: " . current(explode(":", $params->googleTitleFont)) . "
				}"
			);
		}
	}
	
	public static function randomNumber() {
		return rand(1000, 9999);
	}
	
	public static function createId($text, $number) {
		$id = '';
		
		if ($text) {
			$id .= preg_replace('/[^\da-z]/i', '', strtolower($text));
		}
		$id .= $number;
		
		return $id;
	}
	
	public static function escapeHtml($string) {
		return htmlentities($string, ENT_QUOTES, 'UTF-8');
	}
	
	public static function escapeHtmlArray($array) {
		$not_escapable = array(
			'content',
			'item_content',
			'marker_content'
		);
		
		foreach ($array as $key => $val) {
			if (!in_array($key, $not_escapable)) {
				$array[$key] = RSPageBuilderHelper::escapeHtml($val);
			}
		}
		
		return $array;
	}
	
	public static function buildStyle($array) {
		$style				= '';
		
		if (count($array)) {
			$style .= ' style="';
			
			foreach ($array as $key => $val) {
				if ($key == 'background-image') {
					$style .= $key.':url('.$val.');';
				} else {
					$style .= $key.':'.$val.';';
				}
			}
			$style .= '"';
		}
		
		return $style;
	}
	
	public static function elementTypeToTitle($element_type) {
		return JText::_('COM_RSPAGEBUILDER_' . strtoupper(str_replace('rspbld_', '', $element_type)));
	}
	
	public static function toArray($obj) {
		if (is_object($obj)) {
			$obj = (array)$obj;
		}
		if (is_array($obj)) {
			$new_array = array();
			foreach ($obj as $key => $val) {
				$new_array[$key] = RSPageBuilderHelper::toArray($val);
			}
		} else {
			$new_array = $obj;
		}
		
		return $new_array;
	}
	
	public static function init() {
		
	}
}